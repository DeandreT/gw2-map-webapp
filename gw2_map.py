from flask import Flask, render_template, request

app = Flask(__name__)

def get_points():
    with open('coords', 'r') as f:
        lines = f.readlines()
        points = []
        for l in lines:
            x = float(l.split('X:')[1].split(' ')[0])
            y = float(l.split('Y:')[1].strip('Y:').strip())
            timestamp = float(l.split(' ')[0].strip('TS:'))
            points.append([[x, y], timestamp])
        return points[-5000:]
    

@app.route('/selfmap/')
def home():
    if request.method == 'GET':
        points = get_points()
        return render_template('map.html', points=points)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8003')
