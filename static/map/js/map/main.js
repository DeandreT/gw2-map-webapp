var map;

function rezoomSwap() {
    if (sliderControl.options.rezoom === null) {
        sliderControl.options.rezoom = 6;
    } else {
        sliderControl.options.rezoom = null;
    }
}

function teleportMove(e) {
    map.panTo(e.target.options.dest);
}

function unproject(coord) {
    return map.unproject(coord, map.getMaxZoom() );
}

// Main map function
function createMap() {
    map = L.map("mapdiv", {
        minZoom: 2,
        maxZoom: 7,
        crs: L.CRS.Simple
    });
    L.tileLayer("https://tiles.guildwars2.com/1/1/{z}/{x}/{y}.jpg").addTo(map);
    const continent_dims = [49152,49152];
    const mapbounds = new L.LatLngBounds(unproject([0,0]), unproject(continent_dims));
    map.setMaxBounds(mapbounds);
    map.setView(unproject([(continent_dims[0] / 2),(continent_dims[1] / 2)]), 1);
    let l_group = []
    let last_point = unproject(points[0][0]);
    for (let i=0; i < points.length; i++) {
        opacity = Math.max(1/points.length*i, .2);
        point = unproject(points[i][0]);
        timestamp = points[i][1].toString();
        if (point.equals(last_point)) { continue }
        distance = map.distance(last_point, point)
        if (distance > 5) {
            let randomColor = "#" + Math.floor(Math.random()*16777215).toString(16);
            // Add circles to the start/end of teleports
            l_group.push(L.circle(last_point, {color: randomColor, radius: .10, time: timestamp, dest: point}).on('click', teleportMove));
            l_group.push(L.circle(point, {color: randomColor, radius: .15, time: timestamp}));
            // These lines are long distance represent a teleport
            l_group.push(L.polyline([last_point, point], {color: randomColor, opacity: opacity, dashArray: '1 15', time: timestamp, dest: point}).on('click', teleportMove));
                // These lines are short distance movement lines
        } else {
            l_group.push(L.polyline([last_point, point], {color: '#6FCC98', opacity: opacity, time: timestamp, smoothFactor: 10.0}));
        }
        if (i+1 === points.length) { l_group.push(L.marker(point, {title: 'Current Position', time:timestamp})); }
        last_point = point;
    }
    layerGroup = L.layerGroup(l_group).addTo(map);
    sliderControl = L.control.sliderControl({position: "topright", layer:layerGroup, rezoom: null, follow: points.length});
    map.addControl(sliderControl);
    sliderControl.startSlider();
    rezoom = L.control({position: 'bottomleft'});
    rezoom.onAdd = function (map) {
        chkbx = L.DomUtil.create('input', 'rezoom');
        chkbx.type = 'checkbox';
        chkbx.id = 'rezoom';
        return chkbx
    }
    rezoom.addTo(map);
    document.getElementById("rezoom").addEventListener("click", rezoomSwap, true);
}

createMap();
